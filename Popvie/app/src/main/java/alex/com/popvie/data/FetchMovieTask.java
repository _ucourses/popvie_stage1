package alex.com.popvie.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.net.URL;
import java.util.ArrayList;

import alex.com.popvie.generic.AsyncTaskCompleteListener;
import alex.com.popvie.utilities.MovieJsonUtils;
import alex.com.popvie.utilities.NetworkUtils;

/**
 * Created by alexs on 2/14/2017.
 */

public class FetchMovieTask extends AsyncTask<String, Void, ArrayList<Movie>> {

    private static final String POPULAR = "popular";
    private static final String RATED = "rated";

    private Context mContext;
    private AsyncTaskCompleteListener<Movie> mListener;

    public FetchMovieTask(Context context, AsyncTaskCompleteListener<Movie> listener){
        mContext = context;
        mListener = listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        mListener.onTaskStart();
    }

    @Override
    protected ArrayList<Movie> doInBackground(String... values) {
        if(isOnline()) {
            String requestType = POPULAR;
            if (values != null)
                requestType = values[0];

            URL requestUrl = null;
            if (requestType == POPULAR)
                requestUrl = NetworkUtils.buildPopularUrl();
            else if (requestType == RATED)
                requestUrl = NetworkUtils.buildTopRatedUrl();

            try {
                String jsonMoviesResponse = NetworkUtils
                        .getResponseFromHttpUrl(requestUrl);

                ArrayList<Movie> moviesList = MovieJsonUtils.
                        getMovieListFromJson(mContext, jsonMoviesResponse);

                return moviesList;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        else
            return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        super.onPostExecute(movies);
        mListener.onTaskComplete(movies);
    }

    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
