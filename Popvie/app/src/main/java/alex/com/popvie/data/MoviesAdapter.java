package alex.com.popvie.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import alex.com.popvie.R;

/**
 * Created by alexs on 2/11/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieAdapterViewHolder> {

    private static final String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w342/";//w185/";

    private Context mContext;
    private ArrayList<Movie> mMoviesData;

    private final MovieAdapterOnClickHandler mMovieAdapterOnClickHandler;

    public interface MovieAdapterOnClickHandler{
        void onClick(Movie movie);
    }

    public MoviesAdapter(Context context, MovieAdapterOnClickHandler clickHandler) {
        mContext = context;
        mMovieAdapterOnClickHandler = clickHandler;
    }

    @Override
    public MovieAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.movie_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        return new MovieAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieAdapterViewHolder holder, int position) {
        Movie movie = mMoviesData.get(position);
        String imagePath = IMAGE_BASE_URL + movie.poster_path;
        Picasso.with(mContext).load(imagePath).error(R.drawable.ic_missing_pic).into(holder.mMoviePosterImageView);
    }

    @Override
    public long getItemId(int i) {
        return mMoviesData.get(i).id;
    }

    @Override
    public int getItemCount() {
        if(mMoviesData != null)
            return mMoviesData.size();
        else
            return 0;
    }

    public void setMoviesData(ArrayList<Movie> moviesData){
        mMoviesData = moviesData;
        notifyDataSetChanged();
    }

    public class MovieAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ImageView mMoviePosterImageView;

        public MovieAdapterViewHolder(View view) {
            super(view);
            mMoviePosterImageView = (ImageView) view.findViewById(R.id.iv_movie_poster);
            view.setOnClickListener(this);
        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            Movie movie = mMoviesData.get(adapterPosition);
            mMovieAdapterOnClickHandler.onClick(movie);
        }
    }
}
