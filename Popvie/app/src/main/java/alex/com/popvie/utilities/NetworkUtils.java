package alex.com.popvie.utilities;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by alexs on 2/11/2017.
 */

public final class NetworkUtils {

    private static final String MOVIE_API_KEY = "0dad0b43ac7375d8472bcc689184f993";

    private static final String MOVIE_BASE_URL = "http://api.themoviedb.org/3/movie/";

    private static final String POPULAR = "popular";
    private static final String TOP_RATED = "top_rated";

    public static URL buildPopularUrl(){
        return buildUrl(POPULAR);
    }

    public static URL buildTopRatedUrl(){
        return buildUrl(TOP_RATED);
    }

    private static URL buildUrl(String type){
        Uri builtUri = Uri.parse(MOVIE_BASE_URL + type).buildUpon().appendQueryParameter("api_key", MOVIE_API_KEY).build();

        URL url = null;
        try{
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
        return url;
    }

    public static String getResponseFromHttpUrl (URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}