package alex.com.popvie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import alex.com.popvie.data.Movie;

public class DetailsActivity extends AppCompatActivity {

    private static final String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w92/";

    private Movie mMovie;
    private TextView mMovieTitleTextView;
    private TextView mMovieOverviewTextView;
    private ImageView mMovieThumbnail;
    private TextView mMovieRating;
    private TextView mMovieReleaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mMovieTitleTextView = (TextView)findViewById(R.id.tv_movie_title);
        mMovieOverviewTextView = (TextView)findViewById(R.id.tv_movie_overview);
        mMovieRating = (TextView)findViewById(R.id.tv_movie_rate);
        mMovieReleaseDate = (TextView)findViewById(R.id.tv_movie_release);
        mMovieThumbnail = (ImageView)findViewById(R.id.iv_movie_thumbnail);

        Intent previousActivity = getIntent();
        if (previousActivity != null) {
            if (previousActivity.hasExtra(Intent.EXTRA_PACKAGE_NAME)) {
                mMovie = previousActivity.getParcelableExtra(Intent.EXTRA_PACKAGE_NAME);
                mMovieTitleTextView.setText(mMovie.title);
                mMovieOverviewTextView.setText(mMovie.overview);
                mMovieRating.setText(Double.toString(mMovie.vote_average));
                mMovieReleaseDate.setText(mMovie.release_date);
                String thumbnailPath = IMAGE_BASE_URL + mMovie.poster_path;
                Picasso.with(this).load(thumbnailPath).error(R.drawable.ic_missing_pic).into(mMovieThumbnail);
            }
        }
    }
}
