package alex.com.popvie.generic;

import java.util.ArrayList;

/**
 * Created by alexs on 2/14/2017.
 */

public interface AsyncTaskCompleteListener<T> {

    public void onTaskStart();
    public void onTaskComplete(ArrayList<T> result);
}
