package alex.com.popvie.utilities;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import alex.com.popvie.data.Movie;

/**
 * Created by alexs on 2/12/2017.
 */

public class MovieJsonUtils {

    public static ArrayList<Movie> getMovieListFromJson (Context context, String moviesJsonStr) throws JSONException{

        ArrayList<Movie> movies = new ArrayList<>();
        JSONObject moviesJson = new JSONObject(moviesJsonStr);

        if (moviesJson.has("cod")) {
            int errorCode = moviesJson.getInt("cod");

            switch (errorCode) {
                case HttpURLConnection.HTTP_OK:
                    break;
                case HttpURLConnection.HTTP_NOT_FOUND:
                    /* Location invalid */
                    return null;
                default:
                    /* Server probably down */
                    return null;
            }
        }

        JSONArray moviesArray = moviesJson.getJSONArray("results");
        for (int m = 0; m < moviesArray.length(); m++){
            JSONObject movieJson = moviesArray.getJSONObject(m);

            Movie movie = new Movie();
            movie.poster_path = movieJson.getString("poster_path");
            movie.adult = movieJson.getBoolean("adult");
            movie.overview = movieJson.getString("overview");
            movie.release_date = movieJson.getString("release_date");
            movie.id = movieJson.getInt("id");
            movie.original_title = movieJson.getString("original_title");
            movie.original_language = movieJson.getString("original_language");
            movie.title = movieJson.getString("title");
            movie.backdrop_path = movieJson.getString("backdrop_path");
            movie.popularity = movieJson.getDouble("popularity");
            movie.vote_count = movieJson.getInt("vote_count");
            movie.video = movieJson.getBoolean("video");
            movie.vote_average = movieJson.getDouble("vote_average");
            movies.add(movie);
        }

        return movies;
    }
}
