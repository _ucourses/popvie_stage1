package alex.com.popvie;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;

import alex.com.popvie.data.FetchMovieTask;
import alex.com.popvie.data.Movie;
import alex.com.popvie.data.MoviesAdapter;
import alex.com.popvie.generic.AsyncTaskCompleteListener;
import alex.com.popvie.utilities.MovieJsonUtils;
import alex.com.popvie.utilities.NetworkUtils;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.MovieAdapterOnClickHandler {

    private static final String POPULAR = "popular";
    private static final String RATED = "rated";

    private RecyclerView mMoviesRecyclerView;
    private TextView mErrorTextView;
    private MoviesAdapter mMoviesAdapter;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mErrorTextView = (TextView)findViewById(R.id.tv_error);
        mProgressBar = (ProgressBar)findViewById(R.id.pb_loading);

        mMoviesRecyclerView = (RecyclerView)findViewById(R.id.rc_movie_grid);
        GridLayoutManager layoutManager = new GridLayoutManager(this, calculateNoOfColumns(), LinearLayoutManager.VERTICAL, false);
        mMoviesRecyclerView.setLayoutManager(layoutManager);
        mMoviesRecyclerView.setHasFixedSize(true);

        mMoviesAdapter = new MoviesAdapter(this, this);

        mMoviesRecyclerView.setAdapter(mMoviesAdapter);

        new FetchMovieTask(this, new FetchMovieTaskListener()).execute(POPULAR);
        showDataView();
    }

    @Override
    public void onClick(Movie movie) {
        Context context = this;
        Class destinationClass = DetailsActivity.class;
        Intent intentToStartDetailActivity = new Intent(context, destinationClass);
        intentToStartDetailActivity.putExtra(Intent.EXTRA_PACKAGE_NAME, movie);
        startActivity(intentToStartDetailActivity);
    }

    private void showDataView(){
        mMoviesRecyclerView.setVisibility(View.VISIBLE);
        mErrorTextView.setVisibility(View.INVISIBLE);
    }

    private void showErrorView(String errorMessage){
        mMoviesRecyclerView.setVisibility(View.INVISIBLE);
        mErrorTextView.setVisibility(View.VISIBLE);
        mErrorTextView.setText(errorMessage);
    }

    public class FetchMovieTaskListener implements AsyncTaskCompleteListener<Movie> {

        @Override
        public void onTaskStart() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTaskComplete(ArrayList<Movie> result) {
            mProgressBar.setVisibility(View.INVISIBLE);
            if(result != null) {
                mMoviesAdapter.setMoviesData(result);
                showDataView();
            } else if(!isOnline()) {
                showErrorView(getString(R.string.internet_error_message));
            } else {
                showErrorView(getString(R.string.error_message));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movies, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.top_rated) {
            new FetchMovieTask(this, new FetchMovieTaskListener()).execute(RATED);
            showDataView();
            return true;
        }
        else if (id == R.id.most_popular) {
            new FetchMovieTask(this, new FetchMovieTaskListener()).execute(POPULAR);
            showDataView();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private int calculateNoOfColumns() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
